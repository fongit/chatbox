/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Container,
    Header,
    Content,
    Form,
    Item,
    Input,
    Label,
    Text,
    Left,
    Button,
    Icon,
    Body,
    Title,
    Right,
} from 'native-base';
import ChatBox from "./ChatBox";
import Toast from './screen/ToastNB'
import {createStackNavigator, createAppContainer} from 'react-navigation'

type Props = {};

class App extends Component<Props> {
    constructor(){
        super()
        this.state={
            userName:'',
            pwd:'',
            incorrect:false
        }
    }

    handleLogin=()=>{
        const {userName,pwd} = this.state;
        if(userName==='Admin' && pwd ==='123'){
            this.props.navigation.navigate('ChatBox',{userInfo:this.state})
            this.setState({
                incorrect:false
            })
        }else{
            this.setState({
                incorrect:true
            })
        }

    }


    render() {
        return (
            <Container>
                <Header>
                    <Left/>
                    <Body>
                         <Title>Login</Title>
                    </Body>
                    <Right />
                </Header>
                <Content  >
                    <Form >
                        <Item floatingLabel>
                            <Label>Username</Label>
                            <Input
                                onChangeText={(text)=>this.setState({userName:text})}
                            />
                        </Item>
                        <Item floatingLabel last>
                            <Label>Password</Label>
                            <Input secureTextEntry
                                onChangeText={(text)=>this.setState({pwd:text})}
                            />
                        </Item>
                        {this.state.incorrect && <Text style={{color:'red',textAlign:'center',marginTop:10}}>Wrong user name or password!</Text>}
                        <Button style={{marginTop:35}}
                                rounded block info
                                onPress={this.handleLogin}
                        >
                            <Text>Primary</Text>
                        </Button>

                    </Form>
                </Content>

            </Container>
        );

    }
}


const AppNavigator = createStackNavigator({
    Login: {
        screen: App,
        navigationOptions:{
            header:null
        }
    },
    ChatBox: {
        screen: ChatBox,
        navigationOptions:{
            header:null
        }
    }
},{initialRouteName:'ChatBox'}



);

export default createAppContainer(AppNavigator);
