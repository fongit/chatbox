import firebase from 'firebase'
const firebaseConfig = {
    apiKey: "AIzaSyDzg0p3fltCSNcxmCCVmtlLct2IZD4GKgI",
    authDomain: "realtimedb-32b11.firebaseapp.com",
    databaseURL: "https://realtimedb-32b11.firebaseio.com",
    projectId: "realtimedb-32b11",
    storageBucket: "realtimedb-32b11.appspot.com",
    messagingSenderId: "611866912575"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
export default  firebaseApp;